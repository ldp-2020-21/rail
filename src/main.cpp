#include "../include/RailwayManager.h"

int main(){
  RailwayManager rm("./line_description.txt", "./timetables.txt");

  rm.start();

  return 0;
}
