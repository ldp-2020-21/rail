//Author Pietro Girotto matr. 1216355
#include "../include/HighSpeedTrain.h"

HighSpeedTrain::HighSpeedTrain(int number, Station* station, int dir, double pos)
  :Train(number, station, dir, pos)
{
}

int HighSpeedTrain::get_type() const {
  return 2;
}
