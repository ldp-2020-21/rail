//Enrico D'Alberton

#include "../include/Station.h"
#include "../include/Train.h"

Station::Station(std::string stationName, double stationDistance, Station* prevStation, Station* nextStation) {
  name = stationName;
  distance = stationDistance;
  prev = prevStation;
  next = nextStation;
  forwardStandardBinaries = 2;
  reverseStandardBinaries = 2;
  arrivals = {};
  trains = {};
}

Station::~Station() {
  for (int i = 0; i < arrivals.size(); i++) {
    delete[] arrivals[i];
    arrivals[i] = nullptr;
  }
}

void Station::set_timetable(int* arrivalsFromTimetable) {
  arrivals.push_back(arrivalsFromTimetable);
}

void Station::set_trains(std::vector<Train*> trainsFromTimetable) {
  trains = trainsFromTimetable;
}

int const Station::check_free_binary(int trainDirection, int trainType) {
  //se sono la stazione alla fine della corsa faccio passare tutti
  if (next == nullptr || prev == nullptr) {
    return 1;
  }
  if (trainType == 1) { //treno regionale

    if (trainDirection == 1) {
      if (forwardStandardBinaries != 0) {
        forwardStandardBinaries--;
        return 1;
      }
      else {
        return -1;
      }
    }
    else {
      if (reverseStandardBinaries != 0) {
        reverseStandardBinaries--;
        return 1;
      }
      else {
        return -1;
      }
    }

  }
  else { //treno alta velocita'

    if (get_type() == 0) { //stazione locale
      return 0; //passa per i binari di transito
    }
    else { //stazione principale

      if (trainDirection == 1) {
        if (forwardStandardBinaries != 0) {
          forwardStandardBinaries--;
          return 1;
        }
        else {
          return -1;
        }
      }
      else {
        if (reverseStandardBinaries != 0) {
          reverseStandardBinaries--;
          return 1;
        }
        else {
          return -1;
        }
      }

    }
  }
}

void Station::train_leaving_station(Train* train) {

  if (train->get_type() == 1 || get_type() == 1) { //se si tratta di un treno regionale reincremento i contatori dei binari
    train->get_direction() == 1 ? forwardStandardBinaries++ : reverseStandardBinaries++;
  }

  for (int i = 0; i < trains.size(); i++) {
    if (train->get_number() == trains[i]->get_number()) {
      get_next_station(train->get_direction())->add_train_in_arrival(trains[i]); //passo il treno alla stazione successiva
      trains[i] = nullptr;
      trains.erase(trains.begin() + i);
    }
  }
}

void Station::add_train_in_arrival(Train* train) {
  trains.push_back(train);
}

Station* const Station::get_prev_station() {
  return prev;
}

Station* const Station::get_next_station() {
  return next;
}

Station* const Station::get_prev_station(int trainDirection)
{
  if (trainDirection > 0) { //tiene conto della direzione del treno
    return prev;
  }
  else {
    return next;
  }
}

Station* const Station::get_next_station(int trainDirection)
{
  if (trainDirection > 0) { //tiene conto della direzione del treno
    return next;
  }
  else {
    return prev;
  }
}

void Station::set_prev_station(Station* prevStation) {
  prev = prevStation;
}

void Station::set_next_station(Station* nextStation) {
  next = nextStation;
}

int const Station::get_distance() {
  return distance;
}

std::string const Station::get_name() {
  return name;
}

bool const Station::train_too_close(Train* t) {
  double minDistance = distance;
  for (int i = 0; i < trains.size(); i++) {
    if (trains[i]->get_number() == t->get_number()) {
      continue;
    }
    else if (t->get_direction() == trains[i]->get_direction() && trains[i]->get_direction() * (trains[i]->get_position() - minDistance) < 0 && t->get_direction() * (trains[i]->get_position() - t->get_position()) > 0 && trains[i]->get_status() != 1) {
      minDistance = trains[i]->get_position();
    }
  }

  if (t->get_direction() * (minDistance - t->get_position()) < 10.0 && minDistance != distance) { //check se sono troppo vicini
    return true;
  }
  else {
    return false;
  }
}

void Station::add_arrival_time(int* time) {
  std::cout << "Aggiungo orario a " << get_name() << ": " << *(time + 1) << std::endl;
  arrivals.push_back(time);
}

void Station::set_clock(int* clk) {
  clock = clk;
}

int const Station::get_clock()
{
  return *clock;
}

bool const Station::has_trains() {
  if (trains.size() > 0) {
    return true;
  }
  return false;
}

void Station::start_departing_trains() {
  for (int i = 0; i < arrivals.size(); i++) {
    if (*clock == *(arrivals[i] + 1)) { //quando arriva l'ora corretta fa partire i treni
      for (int j = 0; j < trains.size(); j++) {
        if (*arrivals[i] == trains[j]->get_number()) {
          trains[j]->set_status(2);
        }
      }
    }
  }
}

void const Station::update_delay(Train* train)
{
  int i = 0;
  while (i < arrivals.size()) {
    if (arrivals[i][0] == train->get_number()) {
      break;
    }
    i++;
  }

  int delay = *clock - arrivals[i][1];

  if (delay < 0) { //caso in cui il treno e' in orario
    std::cout << "Il treno n. " << train->get_number() << " e' in anticipo di " << delay * (-1) << " minuti rispetto a quanto previsto nella timetable" << std::endl;
  }
  else if (delay > 0) {
    std::cout << "Il treno n. " << train->get_number() << " e' in ritardo di " << delay << " minuti rispetto a quanto previsto nella timetable" << std::endl;
  }

}

void Station::destroy_train(Train* t) {
  for (int i = 0; i < trains.size(); i++) {
    if (t->get_number() == trains[i]->get_number()) {
      delete trains[i];
      trains[i] = nullptr;
      trains.erase(trains.begin() + i);
    }
  }
}