//Author Pietro Girotto matr. 1216355
#include "../include/RegionalTrain.h"

RegionalTrain::RegionalTrain(int number, Station* station, int dir, double pos)
  :Train(number, station, dir, pos)
{
}

int RegionalTrain::get_type() const {
  return 1;
}
