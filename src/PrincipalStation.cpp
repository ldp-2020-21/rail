#include "../include/PrincipalStation.h"

PrincipalStation::PrincipalStation(std::string stationName, double stationDistance, Station* prevStation, Station* nextStation) 
  :Station(stationName, stationDistance, prevStation, nextStation)
{
}

int PrincipalStation::get_type() {
  return 1;
}