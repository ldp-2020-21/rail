//Author Pietro Girotto matr. 1216355
#include "../include/SuperHighSpeedTrain.h"

SuperHighSpeedTrain::SuperHighSpeedTrain(int number, Station* station, int dir, double pos)
  :Train(number, station, dir, pos)
{
}

int SuperHighSpeedTrain::get_type() const {
  return 3;
}

