// Dmytro Tokayev

#include "../include/Timetable.h"

Timetable::Timetable(std::string filePath, Station* firstStation,
                     Station* lastStation, int numOfStations){
  std::ifstream inputFile;
  inputFile.open(filePath);

  if (inputFile.is_open()) {
    std::cout << "Lettura degli orari.\n\n";
    std::string line;
    int trainNumber, trainDirection, trainType, startPosition;
    int i;
    Station* departureStation;

    while (getline(inputFile, line)) {
      std::string tempNumber = "";
      i = 0;

      while (isdigit(line[i])) {
        tempNumber += line[i++];
      }
      i++;
      trainNumber = std::stoi(tempNumber);
      tempNumber = "";
      
      trainDirection = line[i] - '0';
      i += 2;
      
      trainType = line[i] - '0';
      line += " \t";
      i += 2;

      if (trainDirection == 0) {
        startPosition = 0;
        departureStation = firstStation;
        trainDirection = 1;
      } else if (trainDirection == 1) {
        startPosition = lastStation -> get_distance();
        departureStation = lastStation;
        trainDirection = -1;
      } else {
        throw InvalidFileFormatException();
      }

      if (trainType == 1) {
        trains.push_back(new RegionalTrain(trainNumber, departureStation,
                                           trainDirection, startPosition));
      } else if (trainType == 2) {
        trains.push_back(new HighSpeedTrain(trainNumber, departureStation,
                                            trainDirection, startPosition));
      } else if (trainType == 3) {
        trains.push_back(new SuperHighSpeedTrain(trainNumber, departureStation,
                                                 trainDirection, startPosition));
      } else {
        throw InvalidFileFormatException();
      }

      std::cout << "\nN. treno: " << trainNumber << "\t| Partenza: " << trainDirection << "\t| Tipo: " << trainType << "\n";

      Station* tempStation;
      if (trainDirection == 1) {
        tempStation = firstStation;
      } else {
        tempStation = lastStation;
      }
      int prevTime = 0;
      for (int j = 0; j < numOfStations; j++) {
        int* time = new int[2];
        int minTime = 0;
        if (tempStation -> get_prev_station(trains[trains.size() - 1]
                        -> get_direction()) != nullptr) {
          minTime = (trains[trains.size() - 1] -> get_direction() * 60 *
                      (tempStation -> get_distance() -
                      tempStation -> get_prev_station(trains[trains.size() - 1]
                      -> get_direction()) -> get_distance()) /
                      (double)trains[trains.size() - 1] -> max_velocity());
        }
        if (line[i] != '\t') {
          tempNumber = "";
          while (isdigit(line[i])) {
            tempNumber += line[i++];
          }
          i++;
          if (stoi(tempNumber) < prevTime + minTime) {
            tempNumber = std::to_string(prevTime + minTime);
            std::cout << "Timetable e' stata modificata per orario incompatibile.\n";
          }
        } else {
          if (tempNumber == "") {
            tempNumber += "0";
          } else {
            tempNumber = std::to_string(stoi(tempNumber) + minTime);
          }
        }
        *(time) = trainNumber;
        prevTime = stoi(tempNumber);
        *(time + 1) = prevTime;
        tempStation -> add_arrival_time(time);
        time = nullptr;
        tempStation = tempStation -> get_next_station(trainDirection);
      }
    }

    inputFile.close();
    std::cout << std::endl;
  } else {
    throw InvalidFileException();
  }
}

std::vector<Train*> Timetable::get_trains(){
  return trains;
}
