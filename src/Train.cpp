//Author Pietro Girotto matr. 1216355
#include "../include/Train.h"
#include "../include/Station.h"
#include <stdexcept>
#include <iostream>

Train::Train(int number, Station* station, int dir, double pos) {
	trainNumber = number;
	nextStation = station;
	direction = dir;
	position = pos;

	//inizializzazione a -2 per evitare partenze desiderate in RailwayManager
	status = previousStatus = -2;
	delay = 0;
	positionDelay = 0;
	velocity = 0;
}

void Train::update_pos() {
	double increment = static_cast<double>(velocity) / 60;
	position = position + direction * increment;
}

int Train::max_velocity() const {
	switch (get_type()) {
	case 1:
	{
		return 160;
	}
	case 2:
	{
		return 240;
	}
	case 3:
	{
		return 300;
	}
  default:
  {
    return -1;
  }
	}
}

void Train::step() {

	switch (status) {
	case -1: //treno fermo
	{
		//controllo se posso rimettermi in moto
		if (!nextStation->train_too_close(this)) {
			status = previousStatus;
			previousStatus = 0;
    }
		break;
	}

	case 0: //il treno e' in movimento
	{
		//controllo se fermarmi perch� troppo vicino al prossimo treno
		if (nextStation -> train_too_close(this)) {
			previousStatus = status;
			status = -1;
    }
		//controllo se devo avvisare la prossima stazione
    else if (direction * (nextStation->get_distance() - position) < 20.0) {
      alert_next_station();
    }
		//avanzo normalmente
		update_pos();
    break;
	}

	case 1:	//il treno e' in parcheggio
	{
		//provo a contattare la stazione per sapere se riesco a farmi dare un posto
		alert_next_station();
		break;
	}

	case 2: //il treno e' fermo in stazione
	{
		//se devo ancora smaltire i 5 minuti lo faccio
		if (delay > 0) {
			delay--;
		}
		//se ho gi� smaltito i miei 5 min riparto
		else {
			std::cout << "Il treno n. " << trainNumber << " e' partito alle ore " << nextStation->get_clock() << " da " + nextStation->get_name() << " verso " << nextStation->get_next_station(direction)->get_name() << std::endl;
			positionDelay = nextStation->get_distance() + direction * 5.0;
			set_station(nextStation->get_next_station(direction));
			status = 6;
			update_pos();
		}
		break;
	}

	case 3:	//il treno e' da parcheggiare
	{
		//controllo se ho treni davanti a me troppo vicini
    if (nextStation->train_too_close(this)) {
			previousStatus = status;
      status = -1;
    }
		//se sono arrivato al parcheggio ci entro
		else if (direction * (nextStation->get_distance() - position) < 5) {
			position = nextStation->get_distance() - (5.0 * static_cast<double>(direction));
			status = 1;
		}
		//altrimenti avanzo per la mia strada
		else {
			update_pos();
		}
    break;
	}

	case 4: //il treno ha un binario veloce assegnato
	{
		//se sono gi� entro i 5 km dalla stazione punto alla prossima e torno in movimento
		if (direction * (nextStation->get_distance() - position) <= 5) {
			set_station(nextStation->get_next_station(direction));
			status = 0;
		}
		//se ho treni davanti a me mi fermo
		else if (nextStation->train_too_close(this)) {
			previousStatus = status;
			status = -1;
			break;
		}
		//altrimenti avanzo
		else {
			update_pos();
		}
		break;
	}

	case 5: //il treno ha un binario lento assegnato
	{
		//se ho treni davanti a me mi fermo (a meno che non sono a velocit� ridotta
		if (nextStation->train_too_close(this) && velocity != 80) {
			previousStatus = status;
			status = -1;
      break;
		}
		//se sono entro i 5 km dalla stazione rallento
		else if (direction * (nextStation->get_distance() - position) < 5) {
			velocity = 80;
		}
		//se sono arrivato alla stazione mi comporto di conseguenza
		if (direction * (nextStation->get_distance() - position) > 0) {
			position = nextStation->get_distance();
			std::cout << "Il treno n. " << trainNumber << " e' arrivato alla stazione " << nextStation->get_name() << " alle " << nextStation->get_clock() << std::endl;
			status = 2;
			delay = 5;
			nextStation->update_delay(this);
			break;
		}
		update_pos();
		break;
	}

	case 6:	//il treno sta uscendo dalla stazione (era fermo in stazione prima)
	{
		velocity = 80;
		update_pos();
		if (direction * (positionDelay - position) < 0) {
			positionDelay = 0;
			status = 0;
			velocity = max_velocity();
		}
		break;
	}
	}
}

int Train::get_number() const {
	return trainNumber;
}

Station* Train::get_next_station() const {
	return nextStation;
}

void Train::alert_next_station() {
	int response = nextStation->check_free_binary(direction,get_type());
	switch (response) {
		case -1: //non c'� posto 
		{
			//se non � gi� in parcheggio gli dico che dovr� parcheggiarsi
			if (status != 1) {
				std::cout << "Il treno n. " << trainNumber << " segnala alla stazione " << nextStation->get_name();
				std::cout << " di essere in procinto all'arrivo." << std::endl;
				std::cout << "La stazione " << nextStation->get_name() << " risponde di non avere binari liberi e dirigersi verso il parcheggio" << std::endl;
				status = 3;
			}
			break;
		}
		case 0: //binario di transito
		{
			std::cout << "Il treno n. " << trainNumber << " segnala alla stazione " << nextStation->get_name();
			std::cout << " di essere in procinto all'arrivo." << std::endl;
			std::cout << "La stazione " << nextStation->get_name() << " risponde di dirigersi ad un binario di transito" << std::endl;
			status = 4;
			break;
		}
		case 1: //binario lento
		{
			std::cout << "Il treno n. " << trainNumber << " segnala alla stazione " << nextStation->get_name();
			std::cout << " di essere in procinto all'arrivo." << std::endl;
			std::cout << "La stazione " << nextStation->get_name() << " risponde di dirigersi ad un binario standard" << std::endl;
			status = 5;
			break;
		}
    }
}

double Train::get_position() const {
	return position;
}

void Train::set_station(Station* newStation) {
	nextStation->train_leaving_station(this);
	nextStation = newStation;
}

int Train::get_status() const {
	return status;
}

void Train::set_status(const int s) {
	if (s < 0 || s>5) {
		throw std::invalid_argument("Invalid status assignement");
	}
	status = s;
}

int Train::get_direction() const {
	return direction;
}
