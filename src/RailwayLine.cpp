// Dmytro Tokayev

#include "../include/RailwayLine.h"

RailwayLine::RailwayLine(std::string filePath)
    : numOfStations{0}
{
  std::ifstream inputFile;
  inputFile.open(filePath);

  if (inputFile.is_open()) {
    std::cout << "Lettura delle stazioni.\n\n";
    std::string line, stationName;
    int stationType, stationDistance;
    int i = 0;

    getline(inputFile, line);
    line += '\n';
    while (line[i] != '\n') {
      stationName += line[i++];
    }
    firstStation = lastStation = new PrincipalStation(stationName, 0, nullptr, nullptr);

    std::cout << "Prima stazione: " << stationName << std::endl; //
    numOfStations++;

    while (getline(inputFile, line)) {
      stationName = "";
      i = 0;

      while (!isdigit(line[i])) {
        stationName += line[i++];
      }
      stationName.pop_back();

      stationType = line[i] - '0';
      i += 2;

      std::string tempDigit = "";
      while (isdigit(line[i])) {
        tempDigit += line[i++];
      }
      stationDistance = std::stoi(tempDigit);

      if (stationDistance - lastStation -> get_distance() < 20) {
        continue;
      }

      if (stationType == 0) {
        lastStation = new PrincipalStation(stationName, stationDistance, lastStation, nullptr);
      } else if (stationType == 1) {
        lastStation = new LocalStation(stationName, stationDistance, lastStation, nullptr);
      } else {
        inputFile.close();
        throw InvalidFileFormatException();
      }

      lastStation -> get_prev_station() -> set_next_station(lastStation);
      std::cout << "Stazione: " + stationName + "\t\t| Tipo: " << stationType
                << " | Distanza: " << stationDistance << " km\n";
      numOfStations++;
    }

    std::cout << std::endl;

    if (firstStation -> get_next_station() == nullptr) {
      inputFile.close();
      throw InvalidFileFormatException();
    }

    inputFile.close();
  } else {
    throw InvalidFileException();
  }
}

Station* RailwayLine::get_first_station() const{
  return firstStation;
}

Station* RailwayLine::get_last_station() const{
  return lastStation;
}

int RailwayLine::get_num_of_stations() const{
  return numOfStations;
}
