// Dmytro Tokayev

#include "../include/RailwayManager.h"

RailwayManager::RailwayManager(std::string lineDescriptionFilePath,
                               std::string timetablesFilePath)
    : clock {0}
{
  RailwayLine rl(lineDescriptionFilePath);
  firstStation = rl.get_first_station();
  lastStation = rl.get_last_station();
  Timetable ttbl(timetablesFilePath, firstStation,
                 lastStation, rl.get_num_of_stations());
  Station* tempStation = firstStation;
  do {
    tempStation -> set_clock(&clock);
    tempStation = tempStation -> get_next_station();
  } while (tempStation != nullptr);
  trains = ttbl.get_trains();
  for (int i = 0; i < trains.size(); i++) {
    if (trains[i] -> get_direction() == 1) {
      firstStation -> add_train_in_arrival(trains[i]);
    } else {
      lastStation -> add_train_in_arrival(trains[i]);
    }
  }
}

void RailwayManager::start(){
  std::cout << "Inizio programma.\n\n";
    while (trains.size() > 0) {
    update();
  }

  std::cout << "\nFine programma.\n";
}

void RailwayManager::update(){
  if (firstStation -> has_trains()) {
    firstStation -> start_departing_trains();
  }
  if (lastStation -> has_trains()) {
    lastStation -> start_departing_trains();
  }

  for (int i = 0; i < trains.size(); i++) {
    if (trains[i] -> get_status() != -2) {
      trains[i] -> step();
      
      if (trains[i] -> get_status() == 2 && trains[i] -> get_next_station() -> get_next_station(trains[i] -> get_direction()) == nullptr) {
        trains[i] -> get_next_station() -> destroy_train(trains[i]);
        trains[i] = nullptr;
        trains.erase(trains.begin() + i);
      }
    }
  }
  updateClock();
}

void RailwayManager::updateClock(){
  clock++;
}

RailwayManager::~RailwayManager(){
  Station* tempStation = firstStation;
  do {
    tempStation = tempStation -> get_next_station();
    delete tempStation -> get_prev_station();
  } while (tempStation -> get_next_station() != nullptr);
  delete tempStation;
}
