//Author Pietro Girotto matr. 1216355
#ifndef regionaltrain_h
#define regionaltrain_h

#include "Train.h"

class RegionalTrain: public Train {
public:
	RegionalTrain(int number, Station* station, int dir, double pos);
	int get_type() const override;
};

#endif
