//Enrico D'Alberton

#ifndef station_h
#define station_h

#include <string>
#include <vector>
#include <iostream>

class Train;

class Station {
public:
	Station(std::string stationName, double stationDistance, Station* prevStation, Station* nextStation);
	Station(const Station&) = delete; //serve a evitare lo slicing
	Station& operator=(const Station&) = delete; //serve a evitare lo slicing
	~Station(); //distruttore

	void set_timetable(int* arrivalsFromTimetable); //serve a settare gli orari di arrivo dei treni nella singola stazione
	void set_trains(std::vector<Train*> trainsFromTimetable); //viene chiamata solo per la prima stazione per settare tutti i treni in partenza
	int const check_free_binary(int const trainDirection, int const trainType); //restituisce un valore in base alla disponibilit� dei binari:  -1 -> non disponibile, 0 -> transito, 1 -> default
	void train_leaving_station(Train* train); //chiamata quando il treno lascia una qualsiasi stazione, aggiorna vector<Train*> trains della prossima stazione
	void add_train_in_arrival(Train* train); //serve ad aggiungere un treno a vector<Train*> trains
	Station* const get_prev_station(); //restituisce la stazione precedente (da sx a dx)
	Station* const get_next_station(); //restituisce la stazione successiva (da sx a dx)
	Station* const get_prev_station(int const trainDirection); //restituisce la stazione precedente in base all'orientamento del treno
	Station* const get_next_station(int const trainDirection); //restituisce la stazione successiva in base all'orientamento del treno
	void set_prev_station(Station* prevStation); //setta la stazione precedente (da sx a dx)
	void set_next_station(Station* nextStation); //setta la stazione successiva (da sx a dx)
	int const get_distance(); //ritorna la distanza dalla stazione di origine
	std::string const get_name(); //ritorna il nome della stazione
	virtual int get_type() = 0; //ritorna il tipo di stazione  0 -> locale, 1 -> principale
	bool const train_too_close(Train* t); //fa un check per vedere se il treno e' troppo vicino ad un altro treno
	void add_arrival_time(int* time); //Utilizzata dalla classe di Dima
	void set_clock(int* clk); //imposta l'orario usando il puntatore a quello di railwayManager
	int const get_clock(); //ritorna l'orario corrente (in minuti)
	bool const has_trains(); //dice se la stazione ha treni in arrivo 
	void start_departing_trains(); //utilizzata solo all'inizio per far partire i treni dal capolinea
	void const update_delay(Train* train); //comunica in output il ritardo previsto rispetto all'orario di arrivo del treno (quello salvato su arrivals)
	void destroy_train(Train* t); //viene chiamata solo quando il treno arriva al capolinea e distrugge il treno arrivato alla fine 


private:
	Station* prev; //stazione precedente
	Station* next; //stazione successiva
	std::string name; //nome stazione
	std::vector<int*> arrivals; //orari di arrivo dei treni
	std::vector<Train*> trains; //lista di arrivo dei treni
	double distance; //distanza dall'origine
	int forwardStandardBinaries; //binari nel senso dritto
	int reverseStandardBinaries; //binari nel senso contrario
	int* clock; //orario
};

#endif
