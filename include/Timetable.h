// Dmytro Tokayev
/*
 *  La classe Timetable gestisce una tabella degli orari di arrivo dei treni.
 *  Il costruttore accetta una stringa che indica il path al file contenente
 *  il numero del treno, da dove parte, la sua tipologia, orario di partenza
 *  orari di arrivo alle varie stazioni ed infine orario di arrivo al
 *  capolinea.
 */

#ifndef timetable_h
#define timetable_h

#include <fstream>
#include <string>
#include <vector>
#include <iostream>

#include "Station.h"
#include "Train.h"
#include "RegionalTrain.h"
#include "HighSpeedTrain.h"
#include "SuperHighSpeedTrain.h"

class Timetable{
  public:
    Timetable(std::string filePath, Station* firstStation,  // Costruttore
              Station* lastStation, int numOfStations);

    class InvalidFileException{};
    class InvalidFileFormatException{};

    std::vector<Train*> get_trains();   // Restituisce il contenitore dei treni

  private:
    std::vector<Train*> trains;         // Contenitore dei treni
};

#endif
