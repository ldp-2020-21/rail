//Author Pietro Girotto matr. 1216355
#ifndef highspeedtrain_h
#define highspeedtrain_h

#include "Train.h"

class HighSpeedTrain: public Train {
public:
	HighSpeedTrain(int number, Station* station, int dir, double pos);
	int get_type() const override;
};

#endif
