// Dmytro Tokayev

#ifndef railwayline_h
#define railwayline_h

#include <string>
#include <fstream>
#include <iostream>

#include "Station.h"
#include "PrincipalStation.h"
#include "LocalStation.h"

class RailwayLine{
  public:
    RailwayLine(std::string filePath);      // Costruttore
    
    class InvalidFileException{};           // Eccezione per file inesistente
    class InvalidFileFormatException{};     // Eccezione per file con formattazione sbagliata

    int get_num_of_stations() const;        // Restituisce il numero delle stazioni

    Station* get_first_station() const;     // Restituisce la prima stazione
    Station*  get_last_station() const;     // Restituisce la ultima stazione

  private:
    int numOfStations;                      // Numero delle stazioni

    Station* firstStation;                  // Puntatore alla prima stazione
    Station*  lastStation;                  // Puntatore alla ultima stazione
};

#endif
