// Dmytro Tokayev

#ifndef railwaymanager_h
#define railwaymanager_h

#include <iostream>
#include <string>

#include "Station.h"
#include "Train.h"
#include "RailwayLine.h"
#include "Timetable.h"

class RailwayManager{
  public:
    RailwayManager(std::string lineDescriptionFilePath, // Costruttore
                   std::string timetablesFilePath);
    ~RailwayManager();                                  // Distruttore

    void start();                   // Funzione che fa partire il funzionamento
                                    // della linea ferroviaria
  private:
    int clock;                      // Tiene conto dell'ora corrente
    void updateClock();             // Aumenta l'ora di 1 minuto
    
    void update();                  // Funzione eseguita ogni minuto

    std::vector<Train*> trains;     // Contenitore dei treni

    Station* firstStation;          // Puntatore alla prima stazione
    Station* lastStation;           // Puntatore alla ultima stazione
};

#endif
