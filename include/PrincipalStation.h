#ifndef principalstation_h
#define principalstation_h

#include "Station.h"

class PrincipalStation: public Station {
	public:
		PrincipalStation(std::string stationName, double stationDistance, Station* prevStation, Station* nextStation);
		int get_type() override;
};

#endif
