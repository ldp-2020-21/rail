#ifndef localstation_h
#define localstation_h

#include "Station.h"

class LocalStation: public Station {
public:
	LocalStation(std::string stationName, double stationDistance, Station* prevStation, Station* nextStation);
	int get_type() override;
};

#endif
