//Author Pietro Girotto matr. 1216355
#ifndef superhighspeedtrain_h
#define superhighspeedtrain_h

#include "Train.h"

class SuperHighSpeedTrain: public Train {
public:
	SuperHighSpeedTrain(int number, Station* station, int dir, double pos);
	int get_type() const override;
};

#endif
