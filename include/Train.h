//Author Pietro Girotto matr. 1216355
#ifndef train_h
#define train_h

#include "Station.h"

class Train{
public:
	Train(int number, Station* station, int dir, double pos);
	Train(const Train&) = delete; //prevenendo lo slicing
	Train& operator=(const Train&) = delete; //prevenendo lo slicing


	/*
		Fa avanzare il treno a seconda delle diverse casistiche in cui si possa trovare
	*/
	void step();
	

	/*
		Imposta manualmente lo status del treno
	*/
	void set_status(const int s);

	/*
		Ritorna la stazione successiva a cui deve arrivare il treno
	*/
	Station* get_next_station() const;

	/*
		Restituisce la posizione attuale del treno
	*/
	double get_position() const;

	/*
		Ritorna il tipo di treno secondo il seguente codice:
		1: regionale
		2: alta velocita'
		3: alta velocita' super
	*/
	virtual int get_type() const = 0;

	/*
		Ritorna lo status del treno
	*/
	int get_status() const;

	/*
		Restituisce il numero del treno
	*/
	int get_number() const;

	/*
		Restituisce la direzione
	*/
	int get_direction() const;

	/*
		Restituisce la massima velocit� a cui possa andare il treno
	*/
	int max_velocity() const;
	

private:
	/*
		Avverte la stazione a cui si sta avvicinando che � a meno di 20 km e si comporta di conseguenza
	*/
	void alert_next_station();

	/*
		Incrementa la posizione del treno a seconda della velocit� e della direzione
	*/
	void update_pos();

	/*
		Imposta la stazione verso cui si sta dirigendo il treno e avvisa la precedente di starsene andando
	*/
	void set_station(Station* station);


	int trainNumber;	//numero del treno
	double position;	//posizione assoluta del treno
	int status; //status del treno secondo il codice: -2 = deve ancora partire | -1 = fermo | 0 = in corsa | 1 = parcheggiato | 2 = in stazione | 3 = da parcheggiare | 4 = con binario transito assegnato | 5 = con binario lento assegnato | 6 = uscendo dalla stazione
	int previousStatus;	//stato del treno precendete all'essersi fermato
	int delay; //contatore per fermarsi nelle stazioni
	double positionDelay; //segnala fino a quando mantenere velocit� di 80 km dopo una stazione
	int velocity;	//velocit� del treno
	Station* nextStation;	//stazione verso cui il treno si dirige
	int direction; // direzione del treno secondo il codice: 1 = inizio -> fine,  -1 = fine -> inizio
};

#endif
